function isIe(){
	var ua = window.navigator.userAgent;
	var isIE = /MSIE|Trident/.test(ua);
	return isIE;
}

function loadYoutube(selector) {
	var youtube = document.querySelectorAll(selector);
	for (var i = 0; i < youtube.length; i++) {
		
		var el = youtube[i];
	    var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/sddefault.jpg";

	    var image = new Image();
	    image.src = source;
	    image.addEventListener("load", function() {
	        el.appendChild(image);
	    }(i));

	    el.addEventListener("click", function() {

	        var iframe = document.createElement("iframe");
	        iframe.setAttribute("frameborder", "0");
	        iframe.setAttribute("allowfullscreen", "");
	        iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");

	        this.innerHTML = "";
	        this.appendChild(iframe);
	    });
	}
}

function loadVideo(selector) {
	var youtube = document.querySelectorAll(selector);

	for (var i = 0; i < youtube.length; i++) {
		var el = youtube[i];
	    var image = new Image();
	    image.src = el.dataset.poster;
	    image.addEventListener("load", function() {
	        el.appendChild(image);
	    }(i));
	    
	    el.addEventListener("click", function() {
	        var video = document.createElement("video");
	        video.innerHTML = "Sorry, but your browser does not support html5 video.";
	        this.innerHTML = "";
	        this.appendChild(video);
	        
	        video.setAttribute("defaultMuted", true);
	        video.setAttribute("muted", true);
	        video.setAttribute("controls", true);
	        video.setAttribute("autoplay", true);
	        video.setAttribute("src", el.dataset.source);
	        video.muted = true;
	    });
	}
}

function loadMap(el) {
	var key= el.attr("data-mapkey");
	var placeId = el.attr("data-mapplace");
	
	var iframe = document.createElement("iframe");
	iframe.className="map-frame";
	iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("src", "https://www.google.com/maps/embed/v1/place?key="+key+"&q=place_id:"+placeId);
    el.append(iframe);
}


(function($) {
 	//Mobile menu click then remove
	$('.collapse-link').on('click',function() {
	  $('.navbar-collapse').collapse('hide');
	});

	var	$window = $(window),
		$body = $('body');
		
	// Hack: Enable IE flexbox workarounds.
	if (isIe()) {
		$body.addClass('is-ie');
	}

	// Forms.
	// Hack: Activate non-input submits.
	$("#submit-order").on("click", function(event){
		let $form = $("#order-form");
		
		event.preventDefault();
	    event.stopPropagation();
	    $form.addClass("was-validated");

	    let form = $form.get(0); 
        if (form.checkValidity() === false) {
        	return;
        }
        let name = $form.find("#order-name").val();
        let phone = $form.find("#order-phone").val();
        let date = $form.find("#order-date").val();
        let user_agent = window.navigator.userAgent;
		$.post("/km-admin/order.php", 
			{name:name, phone:phone, date:date, user_agent:user_agent},
			function (data){
				if (data === "SUCCESS") {
					$form.find("input").prop("disabled", true);
					$form.find(".order-button").hide();
					$form.find(".alert-success").show();
					$form.find(".alert-danger").hide();
				} else {
					$form.find("input").prop("disabled", false);
					$form.find(".alert-danger").show();
					$form.find(".alert-success").hide();
					$form.find(".order-button").hide();
				}

				console.log("RESPONSE", data);
			},
			"text"
		).fail(function() {
			$form.find("input").prop("disabled", false);
			$form.find(".alert-danger").show();
			$form.find(".alert-success").hide();
			$form.find(".order-button").hide();
  		});
	});


	loadVideo(".embedded-container");
	$("#load-map-button").on("click", function(){
		$(this).hide();
		loadMap($("#map-container-block"));
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
		// $("#map-container-block").append(
		// 				'<iframe class="map-frame"'
		// 				+' frameborder="0" '
		// 				+' src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDCso7zxiqBHyeJYjrp-uSAZt0CdqsNCAI&q=place_id:ChIJ8UfX2vHO1EARPgVhs5wxDFI">'
		// 				+ ' </iframe>'
		// );
	});
	// var scroll = new SmoothScroll('.scroll-link', {
	// 	speed: 500
	// });

})(jQuery);