<?php
namespace KM;

function error_exit($msg) {
    exit("ERROR:".$msg);
}

function success_exit() {
    exit("SUCCESS");
}

function dump(...$args) {
    var_dump($args);
}

function getFormField($name, $required=true) {
    $data = $_POST[$name];
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($required and empty($data)){
      error_exit("FIELD $name REQUIRED"); 
    }
    return $data;
} 

function getFormData($fields) {
    $data = [];
    foreach($fields as $field) {
        $data[$field] = getFormField($field);
    }
    return $data;
}


function sendMail($to, $from, $subject, $message) {
    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=utf-8';

    $headers[] = 'To: '.$to;
    $headers[] = 'From: '.$from;

    //echo var_dump(array($to, $subject, $message, implode("\r\n", $headers)));
    return mail($to, $subject, $message, implode("\r\n", $headers));
}

?>