<?php
namespace KM;

include_once("lib.php");
include_once("db.php");
?>

<html lang="en">

<head>
<style>
    table {
        margin: auto;
        width: 80%;
        border: 3px solid black;
        padding: 10px;
    }
    thead {
        font-weight:bold;
    }
    tr {
    }
    td{
        padding:20px;
    }
    .error{
        color:red;
    }
    .cell{
        max-width: 400px;
    }
</style>
</head>

<body>

<?php
function encrypt($pass) {
    return hash("sha512", $pass);
}

$pass = "5f3c2032428a7c9a54f6081aa1f3d9c85a8a45d027440f6c75cf0a95ae9c66cb018b31fd2c9eefdbc6da9c5c16637a0554f1c503a197938bf99d6d504be62028";
$login = "admin-kravmaga";

function show_orders() {
    $SELECT_QUERY = "SELECT * FROM orders ORDER BY id DESC LIMIT 100";

    $pdo = DB\connect();
    $stmt = $pdo->query($SELECT_QUERY);
    $i = 1;
    echo "<table><thead><tr><td>№</td><td>NAME</td><td>PHONE</td><td>DATE</td><td>AGENT</td><td>IP</td><td>INSERTED</td></tr></thead>";
    while ($row = $stmt->fetch()) {
        $data = [$i, $row['name'], $row['phone'], $row['training_date'], $row['user_agent'], $row['user_ip'], $row['insert_date']];
        $res = "<tr>";
        foreach ($data as $value) {
            $res .= "<td><div class='cell'>$value</div></td>"; 
        }
        $res .= "</tr>";
        echo $res;
        $i++;
    }

    echo "</table>";
    DB\disconnect($pdo);
}

if(empty($_POST)) {
    include("tpl/auth_form.php");
} else {
    $data = getFormData(["name", "password"]);
    if($data['name'] != $login || encrypt($data['password']) != $pass){
        echo("<h1 class='error'/>WRONG CREDENTIALS</h1>");
        include("tpl/auth_form.php");
    } else {
        show_orders();
    }
}

?>
</body>
</html>