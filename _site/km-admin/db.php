<?php
namespace KM\DB;
use \PDO;

require_once "../../_config/config.php";
require_once "lib.php";



function connect() {
    try {
        return new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, DB_OPTIONS);
    } catch (Exception $e) {
        error_log($e->getMessage());
        error_exit("NO DATABASE");
    }
}

function insert($pdo, $query, $data){
    $stmt = $pdo->prepare($query);
    $stmt->execute($data);
    $stmt = null;
}

function disconnect(&$pdo) {
    $pdo = null;
}

?>