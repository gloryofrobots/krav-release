<?php
namespace KM;
header("Content-Type: text/plain");

// simple script for handling training orders and sending emails to the site admin
include_once("consts.php");
include_once("db.php");
include_once("lib.php");

const INSERT_QUERY = "insert into orders (name, phone, training_date, user_agent, user_ip) values (:name, :phone, :date, :user_agent, :user_ip);";

$pdo = DB\connect();

$data = getFormData(["name", "phone", "date", "user_agent"]);
$data["user_ip"] = $_SERVER['REMOTE_ADDR'];

try {
    DB\insert($pdo, INSERT_QUERY, $data);
} catch (Exception $e) {
    error_exit($e->getMessage());
    //error_exit("DB QUERY");
}

DB\disconnect($pdo);
//error_exit();

$MESSAGE = implode(array(
"<div>",
"<h4>Free Lesson Order</h4>",
"<p><b>name:</b> ${data['name']}</p>",
"<p><b>phone:</b> ${data['phone']}</p>",
"<p><b>date:</b> ${data['date']}</p>",
"<p><b>user_agent:</b> ${data['user_agent']}</p>",
"<p><b>user_ip:</b> ${data['user_ip']}</p>",
"</div>"
), "\r\n");

$SUBJECT = "Free Lesson Order";

$result = sendMail(ADMIN_EMAIL, MAILER_NAME, $SUBJECT, $MESSAGE);
if(!$result) {
    error_exit("Can`t send mail");
}

success_exit();

?>